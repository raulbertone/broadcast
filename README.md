Video Swarm is a Cloud-based test environment for browser videoconferencing applications.


# Initial setup

    1. clone this repository on gitlab
    2. install AWS cli on your machine: 
            https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-mac.html
    3. configure AWS cli on your machine:
            https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html
    4. create a key-pair: 
            ssh-keygen -m PEM -t rsa -b 2048 -f key_name
    5. use the following 2 commands to load the key-pair you just created to all AWS regions:
        - AWS_REGIONS="$(aws ec2 describe-regions --query 'Regions[].RegionName' --output text)"
        - for each_region in echo ${AWS_REGIONS} ; do aws ec2 import-key-pair //
          --key-name selenium --public-key-material fileb://”PATH TO YOUR KEY” --region $each_region ; done
    6. copy the AWS image to any region that you want to use (each copy costs a little money,
     so only use carefully)
    7. adapt the CloudFormation template to your account:
        1. in the file cloudformation_stacktemplate.yaml, in line 12 through 42 you have to put the AMI codes 
        (look for it in the AWS EC2 console). 
        The same AMI has a different code for each region in which you copied it
        2. in the file create_stackset.sh, in line 3 put the name of the file that contains the SSH key-pair
        you created earlier


# To run a test

STEP 1: create the AWS instances

At the beginning of the file create_stackset.sh (lines 6 through 29), you find a list of the AWS regions. Here you can specify how many instances you want to create in each region. Example:

	Frankfurt=1,Frankfurt,eu-central-1

creates 1 instance in Frankfurt. Another example:
	
	Oregon=3,Oregon,us-west-2

creates 3 instances in Oregon. DO NOT put a space after the number!
Simply launch the script to create the instances.
Before proceeding with STEP 2, wait until the script has confirmed that all stacks have been created (it takes 2-3 minutes).

STEP 2: bring the instances into a video call

Create a call manually on ers-cloud.de and note down the call_id
Simply launch the java program Swarm.java using a Run Configuration in IntelliJ and follow the instructions that appear in the console:

Note the “Program arguments” field: 
    • the parameter (“ca3d” in the example) is the call_id you noted down earlier

docker-compose logs -f  -t topo-backend
