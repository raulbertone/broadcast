#!/bin/bash

KEY_NAME=20_regions
IS_CALL=true
INSTANCE_BASE=0 # will create this number of instances per location, plus the number specified for each individual location

# African
CapeTown=0,CapeTown,af-south-1

# Europe
Stockholm=2,Stockholm,eu-north-1
Frankfurt=2,Frankfurt,eu-central-1
Paris=2,Paris,eu-west-3
London=2,London,eu-west-2
Ireland=2,Ireland,eu-west-1
Milan=2,Milan,eu-south-1

# Asia Pacific
HongKong=0,HongKong,ap-east-1
Mumbai=0,Mumbai,ap-south-1
Seoul=0,Seoul,ap-northeast-2
Tokyo=0,Tokyo,ap-northeast-1
Singapore=0,Singapore,ap-southeast-1
Sydney=0,Syndey,ap-southeast-2
Bahrain=0,Bahrain,me-south-1

# Sud America
Sao_Paulo=0,Sao_Paulo,sa-east-1

# Canada
Montreal=0,Montreal,ca-central-1

# USA
Virginia=0,Virginia,us-east-1
Ohio=0,Ohio,us-east-2
California=0,California,us-west-1
Oregon=0,Oregon,us-west-2

RED='\033[0;31m'
NC='\033[0m' # No Colour

regions=($CapeTown $Bahrain $Milan $HongKong $Stockholm $Frankfurt $Paris $London $Ireland $Mumbai $Seoul $Tokyo $Singapore $Sydney $Sao_Paulo $Montreal $Virginia $Ohio $California $Oregon)

stack_name=$(head /dev/urandom | tr -dc A-Za-z | head -c10)

if [ $INSTANCE_BASE -gt 0 ]
then
  echo -e ${RED}WARNING: INSTANCE_BASE is greater than 0. Proceed with caution!${NC}
  read -n 1 -s -r -p "Press any key to continue"
  echo ""
fi

echo "Creating stacks..."

# $1 -> stack size
# $2 -> region name
# $3 -> city name
function create_stack {
  file=""
  if [ "$IS_CALL" = true ] ; then
    file="file://call_stacktemplate.yaml"
  else
    file="file://MPI_stacktemplate.yaml"
  fi

  let "total_size = $1 + INSTANCE_BASE"
  output=$(aws cloudformation create-stack \
      --stack-name $stack_name \
      --template-body $file \
      --parameters ParameterKey=SSHkey,ParameterValue=$KEY_NAME ParameterKey=Size,ParameterValue=$total_size \
      --region $2)
  StackId=$(echo $output | cut -d '"' -f 4)

   (
    if aws cloudformation wait stack-create-complete \
      --stack-name $StackId \
      --region $2
    then
      echo "$3 stack created"
    else
      echo "$3 stack creation FAILED"
    fi
   ) &
}

for region in "${regions[@]}"
do
  # sets the delimiter (only for the following command, i.e. read)
  # then runs read on the passed (<<<) string
  # saving it in the variable "array" as an array (-a option)
  IFS=',' read -ra array <<< "$region"

  if [ ${array[0]} -gt 0 ] || [ $INSTANCE_BASE -gt 0 ]
  then
    echo "Creating a stack with $(( array[0] + $INSTANCE_BASE)) instance(s) in ${array[1]}..."
    create_stack ${array[0]} ${array[2]} ${array[1]}
  fi
done

VAR=""
while [[ "$VAR" != "delete" ]]
do
    echo "At anytime, to delete all stacks write ´delete´ and press ENTER"
    read VAR
done

# $1 -> region name
# $2 -> city name
function delete_stack {
  aws cloudformation delete-stack \
      --stack-name $stack_name \
      --region $1
  echo "$2 stack deleted"
}

for region in "${regions[@]}"
do
  # sets the delimiter (only for the following command, i.e. read)
  # then runs read on the passed (<<<) string
  # saving it in the variable "array" as an array (-a option)
  IFS=',' read -ra array <<< "$region"

  if [ ${array[0]} -gt 0 ] || [ $INSTANCE_BASE -gt 0 ]
  then
    delete_stack ${array[2]} ${array[1]}
  fi
done