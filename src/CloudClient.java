import geolocation.GeoInformation;
import geolocation.IpGeolocationAdapter;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;

import javax.json.Json;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.*;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class CloudClient {

    private String name;
    private WebDriver driver;
    private Map<String, Object> vars;
    private String host;
    private static String basePath = "./screenshots/";
    JavascriptExecutor js;

    public CloudClient(String name){
        this.name = name;
    }

    public void setUp() throws MalformedURLException {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--headless");
        //options.addArguments("--verbose");

        // version 87 options
        options.addArguments("--remote-debugging-port=9222"); //Very important to use with chromedriver 87
        options.addArguments("--allow-file-access-from-files");
        options.addArguments("--use-fake-device-for-media-stream");
        options.addArguments("--use-fake-ui-for-media-stream"); //to allow to use mic even if not used

        // added to try and get the logs
        options.addArguments("--log-path=chrome.log");
        options.addArguments("--enable-logging"); // =stderr");
        options.addArguments("--v=1"); // > raulchromelog.txt 2>&1");

        driver = new RemoteWebDriver(new URL("http://194.94.82.240:9990/wd/hub"), options);
        js = (JavascriptExecutor) driver;
        vars = new HashMap<String, Object>();
    }

    public void join(String call_id, boolean hideVideo) {
        System.out.print("Loading page...");
        driver.get("https://ers-cloud.de:8080/");
        System.out.println("done.");

        System.out.print("Resizing window...");
        driver.manage().window().setSize(new Dimension(1920, 1080));
        System.out.println("done.");

        System.out.print("Selecting join...");
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.findElement(By.id("joinRadio")).click();
        System.out.println("done.");

        System.out.print("Registering...");
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.findElement(By.id("registerBtn")).click();
        System.out.println("done.");

        System.out.print("Setting the call_id...");
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.findElement(By.id("call_idDisplay2")).click();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.findElement(By.id("call_idDisplay2")).sendKeys(call_id);
        System.out.println("done.");

        System.out.print("Joining the call...");
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.findElement(By.id("sendButton")).click();
        System.out.println("done.");

        /*
        System.out.print("Hiding the video...");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("muteVideoIcon")).click();
        System.out.println("done.");

         */
    }

    public void join(String call_id, String node_id, boolean hideVideo) {
        System.out.print("Loading page...");
        driver.get("https://ers-cloud.de:8080/");
        System.out.println("done.");

        System.out.print("Resizing window...");
        driver.manage().window().setSize(new Dimension(1920, 1080));
        System.out.println("done.");

        System.out.print("Selecting join...");
        driver.findElement(By.id("joinRadio")).click();
        System.out.println("done.");

        System.out.print("Registering...");
        driver.findElement(By.id("registerBtn")).click();
        System.out.println("done.");

        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // expand menu to simulate receiving msg from Atop server
        System.out.print("Expanding Atop Msgs...");
        driver.findElement(By.id("atopMsgBtn")).click();
        System.out.println("done.");

        // wait for the drop down menu to be expanded to see the radios
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Selecting Call Neighbour from simulated Atop Msgs
        System.out.print("Selecting Call...");
        driver.findElement(By.id("callRadio")).click();
        System.out.println("done.");

        //Set text area using call_id and node_id
        System.out.print("Setting the call_id: " + call_id + "...");
        driver.findElement(By.id("msgCallid")).click();
        driver.findElement(By.id("msgCallid")).sendKeys(call_id);
        System.out.println("done.");

        System.out.print("Setting the node_id: " + node_id + "...");
        driver.findElement(By.id("msgNodeid")).click();
        driver.findElement(By.id("msgNodeid")).sendKeys(node_id);
        System.out.println("done.");

        //Receive simu msg from atop and call neighbour
        System.out.print("Setting Msg...");
        driver.findElement(By.id("topomsgSetCallUserBtn")).click();
        System.out.println("done.");

        //Receive simu msg from atop and call neighbour
        System.out.print("Calling Neighbour...");
        driver.findElement(By.id("topomsgButton")).click();
        System.out.println("done.");
    }

    public void tearDown(String call) {
        if (!call.equals("noCall")) {
            try {
                System.out.print("Client " + name + " leaving the call...");
                driver.findElement(By.id("leaveButton")).click();
                System.out.println("done.");
            } catch(StaleElementReferenceException e){
                System.out.println(e.getMessage());
            }
        }

        driver.quit();
    }

    public String getIP(){
        SessionId session = ((RemoteWebDriver)driver).getSessionId();

        URL url = null;
        try {
            url = new URL("http://194.94.82.240:9990/grid/api/testsession?session=" + session.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        StringBuilder sb = new StringBuilder();
        String line = null;
        while (true) {
            try {
                if (!((line = reader.readLine()) != null)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            sb.append(line);
        }

        JsonReader jreader = Json.createReader(new StringReader(sb.toString()));
        JsonObject jsonMsg = null;
        try {
            jsonMsg = jreader.readObject();
        } catch (JsonException e) {
        }
        jreader.close();

        String clientUrl = jsonMsg.getString("proxyId");
        String ip = clientUrl.substring(7).split(":")[0];

        InetAddress addr = null;
        try {
            addr = InetAddress.getByName(ip);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        host = addr.getHostName();

        GeoInformation geoLocation = IpGeolocationAdapter.lookup(ip);
        String location = "empty";
        if(host.contains("eu-south-1")){
            location="Milan";
        } else if(host.contains("af-south-1")){
            location="Cape Town";
        } else if(!geoLocation.getCity().equals("")) {
            location = geoLocation.getCity();
        } else if(!geoLocation.getProvince().equals("")) {
            location = geoLocation.getProvince();
        } else if(!geoLocation.getCountry().equals("")) {
            location = geoLocation.getCountry();
        } else if(!geoLocation.getContinent().equals("")) {
            location = geoLocation.getContinent();
        }

        Swarm.clients.put(location, host);

        return "IP: " + ip + " DNS: " + host;
    }

    public void takeScreenshot(){
        String path = host + ".png";
        Path filePath = Paths.get(basePath + path);
        byte[] scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        try {
            Files.deleteIfExists(filePath);
            Files.createFile(filePath);
            Files.write(filePath, scrFile, StandardOpenOption.WRITE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
