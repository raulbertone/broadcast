import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedList;
import java.util.List;

public class Scratchpad {

    public static void main(String[] args){
        List<String> hostNames = new LinkedList<>();
        hostNames.add("firstHost");
        hostNames.add("second");
        Path file = Paths.get("aws_hosts.txt");
        try {
            Files.write(file, hostNames, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
