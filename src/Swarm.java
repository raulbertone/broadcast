import javax.json.Json;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

public class Swarm {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLUE = "\u001B[34m";
    private static Path hostFilePath = Paths.get("AWS_hosts.txt");
    private static final String[] locations = {"Cape Town","Manama","Hong Kong","Frankfurt","London","Mumbai","Seoul","Stockholm","Dublin","Winnipeg","Tokyo","Virginia","Sao Paulo","Oregon","Singapore","Paris","Sydney","Ohio","California","Milan"};
    public static final Map<String, String> clients = new HashMap<>();
    private static LinkedList<CloudClient> cloudClients = new LinkedList<>();
    private static String node_id = "";

    public static void main(String[] args) {

        // create hostfile and locations file
        try {
            Files.deleteIfExists(hostFilePath);
            Files.createFile(hostFilePath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        while(true){
            takeScreenshots();

            int availableClients = freeSlots();
            if(availableClients > 0){
                System.out.println("There are " + availableClients + " available slots in AWS");
            } else {
                System.out.println("WANING: there are no more available slots in AWS. Run 'create_stack.sh' again to create more slots.");
                System.out.println("Do you want to check again (´N´ exits the program)? Y/N");

                try {
                    char userInput = (char)System.in.read();
                    if(userInput == 'y' || userInput == 'Y'){
                        System.in.skip(100);
                        continue;
                    } else if (userInput == 'n' || userInput == 'N'){
                        System.in.skip(100);
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            int num = 0;
            while(true){
                System.out.println("How many clients do you want to create? (\"n\" terminates all clients and exits the program)");
                Scanner in = new Scanner(System.in);
                try {
                    num = in.nextInt();
                } catch (InputMismatchException e) {
                    String input = in.nextLine();
                    if(input.equals("n") || input.equals("N")){
                        terminateClients(args[0]);
                        System.exit(0);
                    } else {
                        System.out.println("ERROR: invalid input (not an integer number). Try Again.");
                        continue;
                    }
                }

                if(num < 0) {
                    System.out.println("ERROR: invalid input (negative number). Try again.");
                    continue;
                } else if(num <= availableClients){
                    break;
                } else {
                    System.out.println("Not enough available slots in AWS! (run 'create_stack.sh' again to create more slots)");
                }
            }

            for(; num>0; num--){
                CloudClient client = new CloudClient(String.valueOf(cloudClients.size() + 1));
                cloudClients.add(client);

                // N-to-1
                if(args.length == 2 && args[1].equals("Nto1")) {
                    if(cloudClients.size() == 1){
                        System.out.print("Setting up the central client...");
                        setUpClient(client);
                        client.join(args[0], true);

                        System.out.println("Please, insert the node_id of the central node:");
                        Scanner in = new Scanner(System.in);
                        node_id = in.nextLine();
                    } else {
                        System.out.print("Setting up load client # " + (cloudClients.size() - 1) + "...");
                        setUpClient(client);
                        client.join(args[0], node_id, true);
                    }
                // broadcast
                } else {
                    System.out.print("Setting up client # " + cloudClients.size() + "...");
                    setUpClient(client);

                    if(!args[0].equals("noCall")){
                        client.join(args[0], true);
                    }
                }
            }
        }

        System.out.print("Saving the hostnames to file...");
        for(String s: locations){
            String host="";

            host = clients.get(s);

            try {
                Files.write(hostFilePath, (host + "," + s + "\n").getBytes(), StandardOpenOption.WRITE, StandardOpenOption.APPEND);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("done");

        terminateClients(args[0]);
    }

    private static void terminateClients(String call){
        System.out.print("Terminating the clients...");
        for(CloudClient c: cloudClients){
            c.tearDown(call);
        }
        System.out.println("done");
    }

    private static void setUpClient(CloudClient client){
        try {
            client.setUp();
            System.out.println("done");
            System.out.println(ANSI_BLUE + client.getIP() + ANSI_RESET);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static int freeSlots(){
        URL url = null;
        try {
            url = new URL("http://194.94.82.240:9990/grid/api/hub");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        StringBuilder sb = new StringBuilder();
        String line = null;
        while (true) {
            try {
                if (!((line = reader.readLine()) != null)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            sb.append(line);
        }

        JsonReader jreader = Json.createReader(new StringReader(sb.toString()));
        JsonObject jsonMsg = null;
        try {
            jsonMsg = jreader.readObject();
        } catch (JsonException e) {
        }
        jreader.close();

        return jsonMsg.getJsonObject("slotCounts").getInt("free");
    }

    private static void takeScreenshots(){
        if(cloudClients.size() > 0){
            System.out.println("Do you want to take screenshots of all the current clients? (Y/N)");

            try {
                char userInput = (char)System.in.read();
                if(userInput == 'y' || userInput == 'Y'){
                    System.out.print("Saving screenshots...");
                    for(CloudClient c: cloudClients){
                        c.takeScreenshot();
                    }
                    System.out.println("done");
                }

                System.in.skip(100);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}
