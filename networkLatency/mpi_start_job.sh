#!/bin/bash

mv ../AWS_hosts.txt ./
rm ./results/*.txt

file=AWS_hosts.txt
while read -r line ; do # -r to avoid mangling baskslashes (not that there will be any in this case)
   (
    host=$(echo $line | cut -d ',' -f 1)
    city=$(echo $line | cut -d ',' -f 2)
    echo "Starting job in ${city}..."
    scp -q -o StrictHostKeyChecking=no AWS_hosts.txt ping_hosts.sh ubuntu@"$host":/tmp # copy the host file and ping script to each instance
    ssh -q -o StrictHostKeyChecking=no -n ubuntu@"$host" "chmod 775 /tmp/ping_hosts.sh && /tmp/ping_hosts.sh" # ping all hosts
    scp -q -o StrictHostKeyChecking=no ubuntu@"$host":/tmp/ping_results.txt ./results/"${city}"_results.txt # fetch the results
    echo "Finished job in ${city}."
   ) &
done < ${file} # the input to the whole loop is declared here. Neat...

echo "Done"