#!/bin/bash

filename="net_latency_$(date "+%Y.%m.%d-%H.%M.%S").csv"

echo ",$(cat locations.txt)" >> ./results/saved/$filename

IFS=',' read -r -a array <<< "$(cat locations.txt)"

i=0
while [ $i -lt 20 ]; do
    echo "${array[$i]},$(cat "./results/${array[$i]}_results.txt")" >> ./results/saved/$filename
    i=$((i+1))
done