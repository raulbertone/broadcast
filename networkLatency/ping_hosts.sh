#!/bin/bash

file=/tmp/AWS_hosts.txt
> /tmp/ping_results.txt # clear the output file

results=""
while read -r line ; do # -r to avoid mangling baskslashes (not that there will be any in this case)
  host=$(echo $line | cut -d ',' -f 1)
  ping_out=$(ping -c 10 -q "$host")
  results="$results$(echo -n "$ping_out" | grep rtt | cut -d '/' -f 5),"
done < ${file} # the input to the whole loop is declared here. Neat...

  echo $results >> /tmp/ping_results.txt
