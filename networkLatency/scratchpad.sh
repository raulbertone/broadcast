#!/bin/bash

echo ",$(cat locations.txt)" >> ./prova.csv

IFS=',' read -r -a array <<< "$(cat locations.txt)"

i=0
while [ $i -lt 19 ]; do
    echo "${array[$i]},$(cat "./results/${array[$i]}_results.txt")" >> ./prova.csv
    i=$((i+1))
done